
    array = [1, "anu", 6, 5, "anu", "ram", 6, "aman", "anu"]

    numbers_array = []
    array.each do |e|
      numbers_array << e if e == e.to_i
    end

    numbers_array.sort!

    strings_array = []
    array.each do |e|
        strings_array << e if e == e.to_s
    end

    strings_array.sort!

    final_array = []
    final_array << numbers_array << strings_array
    final_array.flatten!
    final_array.uniq!

    p final_array
